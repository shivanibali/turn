package mcgill.xtext.turn.diagram;

import static org.junit.Assert.*;

import org.eclipse.emf.common.util.EList;
import org.junit.Test;
import org.xtext.example.mydsl.tURN.Actor;
import org.xtext.example.mydsl.tURN.Contribution;
import org.xtext.example.mydsl.tURN.ContributionType;
import org.xtext.example.mydsl.tURN.Decomposition;
import org.xtext.example.mydsl.tURN.DecompositionType;
import org.xtext.example.mydsl.tURN.Dependency;
import org.xtext.example.mydsl.tURN.ElementLink;
import org.xtext.example.mydsl.tURN.Evaluation;
import org.xtext.example.mydsl.tURN.EvaluationStrategy;
import org.xtext.example.mydsl.tURN.IntentionalElement;
import org.xtext.example.mydsl.tURN.IntentionalElementType;
import org.xtext.example.mydsl.tURN.TURNFactory;
import org.xtext.example.mydsl.tURN.URNspec;
import org.xtext.example.mydsl.tURN.impl.TURNFactoryImpl;

public class testing2 {
	
	
//	@Test
//	public void dependencyLinksTwoLevels() {
//
//		TURNFactory factory = new TURNFactoryImpl();
//
//		// URNSPEC
//		URNspec urnSpec = factory.createURNspec();
//		urnSpec.setName("School");
//
//		// ACTOR
//		Actor actor = factory.createActor();
//		actor.setName("Teacher");
//		urnSpec.getActors().add(actor);
//
//		// INTENTIONAL ELEMENT
//		IntentionalElement elem1 = factory.createIntentionalElement();
//		elem1.setType(IntentionalElementType.SOFTGOAL);
//		elem1.setName("Improve Performance");
//		actor.getElems().add(elem1);
//
//		IntentionalElement elem2 = factory.createIntentionalElement();
//		elem2.setType(IntentionalElementType.GOAL);
//		elem2.setName("Increase Trust");
//		actor.getElems().add(elem2);
//
//		IntentionalElement elem3 = factory.createIntentionalElement();
//		elem3.setType(IntentionalElementType.GOAL);
//		elem3.setName("Increase Motivation");
//		actor.getElems().add(elem3);
//		
//		IntentionalElement elem4 = factory.createIntentionalElement();
//		elem4.setType(IntentionalElementType.BELIEF);
//		elem4.setName("Inspire Students");
//		actor.getElems().add(elem4);
//		
//		IntentionalElement elem5 = factory.createIntentionalElement();
//		elem5.setType(IntentionalElementType.BELIEF);
//		elem5.setName("Better Output");
//		actor.getElems().add(elem5);
//		
//		// DEPENDENCY ELEMENT LINK
//		Dependency dependency1 = factory.createDependency();
//		dependency1.setDest(elem2);
//		elem1.getLinksSrc().add(dependency1);
//		
//		Dependency dependency2 = factory.createDependency();
//		dependency2.setDest(elem3);
//		elem1.getLinksSrc().add(dependency2);
//		
//		Dependency dependency3 = factory.createDependency();
//		dependency3.setDest(elem4);
//		elem2.getLinksSrc().add(dependency3);
//		
//		Dependency dependency4 = factory.createDependency();
//		dependency4.setDest(elem5);
//		elem2.getLinksSrc().add(dependency4);
//		
//		// STRATEGY
//		EvaluationStrategy strategy = factory.createEvaluationStrategy();
//		strategy.setName("StrategyFirst");
//		urnSpec.getStrategies().add(strategy);
//
//		// EVALUATION
//		Evaluation eval1 = factory.createEvaluation();
//		eval1.setIntElement(elem2);
//		eval1.setEvaluation(50);
//		strategy.getEvaluations().add(eval1);
//		
//		Evaluation eval2 = factory.createEvaluation();
//		eval2.setIntElement(elem3);
//		eval2.setEvaluation(-75);
//		strategy.getEvaluations().add(eval2);
//		
//		Evaluation eval3 = factory.createEvaluation();
//		eval3.setIntElement(elem4);
//		eval3.setEvaluation(80);
//		strategy.getEvaluations().add(eval3);
//		
//		Evaluation eval4 = factory.createEvaluation();
//		eval4.setIntElement(elem5);
//		eval4.setEvaluation(50);
//		strategy.getEvaluations().add(eval4);
//
//		TURNEvaluationManager evalManager = new TURNEvaluationManager(strategy);
//
//		assertEquals(-75, evalManager.getEvaluation(elem1));
//		assertEquals(50, evalManager.getEvaluation(elem2));
//		assertEquals(-75, evalManager.getEvaluation(elem3));
//		assertEquals(80, evalManager.getEvaluation(elem4));
//		assertEquals(50, evalManager.getEvaluation(elem5));
//	}
	
//	@Test
//	public void dependencyContributionLinkSet() {
//
//		TURNFactory factory = new TURNFactoryImpl();
//
//		// URNSPEC
//		URNspec urnSpec = factory.createURNspec();
//		urnSpec.setName("School");
//
//		// ACTOR
//		Actor actor = factory.createActor();
//		actor.setName("Teacher");
//		urnSpec.getActors().add(actor);
//		
//		Actor actor2 = factory.createActor();
//		actor2.setName("Student");
//		urnSpec.getActors().add(actor2);
//
//		// INTENTIONAL ELEMENT
//		IntentionalElement elem1 = factory.createIntentionalElement();
//		elem1.setType(IntentionalElementType.SOFTGOAL);
//		elem1.setName("Improve Performance");
//		actor.getElems().add(elem1);
//
//		IntentionalElement elem2 = factory.createIntentionalElement();
//		elem2.setType(IntentionalElementType.GOAL);
//		elem2.setName("Increase Trust");
//		actor2.getElems().add(elem2);
//
//		IntentionalElement elem3 = factory.createIntentionalElement();
//		elem3.setType(IntentionalElementType.GOAL);
//		elem3.setName("Increase Knowledge");
//		actor.getElems().add(elem3);
//		
//		IntentionalElement elem4 = factory.createIntentionalElement();
//		elem4.setType(IntentionalElementType.BELIEF);
//		elem4.setName("Better Output");
//		actor2.getElems().add(elem4);
//		
//		// DEPENDENCY CONTRIBUTION ELEMENT LINK
//		Dependency dependency1 = factory.createDependency();
//		dependency1.setDest(elem2);
//		elem1.getLinksSrc().add(dependency1);
//		
//		Dependency dependency2 = factory.createDependency();
//		dependency2.setDest(elem4);
//		elem3.getLinksSrc().add(dependency2);
//		
//		Contribution contribution1 = factory.createContribution();
//		contribution1.setDest(elem2);
//		contribution1.setQuantitativeContribution(95);
//		elem4.getLinksSrc().add(contribution1);
//		
//		// STRATEGY
//		EvaluationStrategy strategy = factory.createEvaluationStrategy();
//		strategy.setName("StrategyFirst");
//		urnSpec.getStrategies().add(strategy);
//
//		// EVALUATION
//		Evaluation eval1 = factory.createEvaluation();
//		eval1.setIntElement(elem3);
//		eval1.setEvaluation(75);
//		strategy.getEvaluations().add(eval1);
//		
//		Evaluation eval2 = factory.createEvaluation();
//		eval2.setIntElement(elem4);
//		eval2.setEvaluation(80);
//		strategy.getEvaluations().add(eval2);
//
//		TURNEvaluationManager evalManager = new TURNEvaluationManager(strategy);
//
//		assertEquals(0, evalManager.getEvaluation(elem1));
//		assertEquals(76, evalManager.getEvaluation(elem2));
//		assertEquals(75, evalManager.getEvaluation(elem3));
//		assertEquals(80, evalManager.getEvaluation(elem4));
//	}
	
//	@Test
//	public void decompositionDependencyLinksTwoLevels() {
//
//		TURNFactory factory = new TURNFactoryImpl();
//
//		// URNSPEC
//		URNspec urnSpec = factory.createURNspec();
//		urnSpec.setName("School");
//
//		// ACTOR
//		Actor actor = factory.createActor();
//		actor.setName("Teacher");
//		urnSpec.getActors().add(actor);
//		
//		Actor actor2 = factory.createActor();
//		actor2.setName("Student");
//		urnSpec.getActors().add(actor2);
//
//		// INTENTIONAL ELEMENT
//		IntentionalElement elem1 = factory.createIntentionalElement();
//		elem1.setType(IntentionalElementType.SOFTGOAL);
//		elem1.setName("Improve Performance");
//		actor.getElems().add(elem1);
//
//		IntentionalElement elem2 = factory.createIntentionalElement();
//		elem2.setType(IntentionalElementType.GOAL);
//		elem2.setName("Increase Trust");
//		actor.getElems().add(elem2);
//
//		IntentionalElement elem3 = factory.createIntentionalElement();
//		elem3.setType(IntentionalElementType.GOAL);
//		elem3.setName("Increase Knowledge");
//		actor.getElems().add(elem3);
//		
//		IntentionalElement elem4 = factory.createIntentionalElement();
//		elem4.setType(IntentionalElementType.BELIEF);
//		elem4.setName("Better Output");
//		actor2.getElems().add(elem4);
//		
//		IntentionalElement elem5 = factory.createIntentionalElement();
//		elem5.setType(IntentionalElementType.GOAL);
//		elem5.setName("Inspire Students");
//		actor2.getElems().add(elem5);
//		
//		IntentionalElement elem6 = factory.createIntentionalElement();
//		elem6.setType(IntentionalElementType.BELIEF);
//		elem6.setName("High Attendance");
//		actor.getElems().add(elem6);
//		
//		// DECOMPOSITION DEPENDENCY ELEMENT LINK
//		Decomposition decomposition = factory.createDecomposition();
//		decomposition.setDest(elem3);
//		decomposition.setDecompositionType(DecompositionType.AND);
//		elem1.getLinksSrc().add(decomposition);
//
//		Decomposition decomposition2 = factory.createDecomposition();
//		decomposition2.setDest(elem3);
//		decomposition2.setDecompositionType(DecompositionType.AND);
//		elem2.getLinksSrc().add(decomposition2);
//		
//		Dependency dependency1 = factory.createDependency();
//		dependency1.setDest(elem4);
//		elem3.getLinksSrc().add(dependency1);
//		
//		Decomposition decomposition3 = factory.createDecomposition();
//		decomposition3.setDest(elem6);
//		decomposition3.setDecompositionType(DecompositionType.OR);
//		elem3.getLinksSrc().add(decomposition3);
//		
//		Dependency dependency2 = factory.createDependency();
//		dependency2.setDest(elem5);
//		elem6.getLinksSrc().add(dependency2);
//		
//		// STRATEGY
//		EvaluationStrategy strategy = factory.createEvaluationStrategy();
//		strategy.setName("StrategyFirst");
//		urnSpec.getStrategies().add(strategy);
//
//		// EVALUATION
//		Evaluation eval1 = factory.createEvaluation();
//		eval1.setIntElement(elem1);
//		eval1.setEvaluation(80);
//		strategy.getEvaluations().add(eval1);
//		
//		Evaluation eval2 = factory.createEvaluation();
//		eval2.setIntElement(elem2);
//		eval2.setEvaluation(65);
//		strategy.getEvaluations().add(eval2);
//		
//		Evaluation eval3 = factory.createEvaluation();
//		eval3.setIntElement(elem4);
//		eval3.setEvaluation(70);
//		strategy.getEvaluations().add(eval3);
//
//		TURNEvaluationManager evalManager = new TURNEvaluationManager(strategy);
//
//		assertEquals(80, evalManager.getEvaluation(elem1));
//		assertEquals(65, evalManager.getEvaluation(elem2));
//		assertEquals(65, evalManager.getEvaluation(elem3));
//		assertEquals(70, evalManager.getEvaluation(elem4));
//		assertEquals(0, evalManager.getEvaluation(elem5));
//		assertEquals(0, evalManager.getEvaluation(elem6));
//	}
	@Test
	public void mixedElementLinksTwoLevels() {

		TURNFactory factory = new TURNFactoryImpl();

		// URNSPEC
		URNspec urnSpec = factory.createURNspec();
		urnSpec.setName("School");

		// ACTOR
		Actor actor = factory.createActor();
		actor.setName("Teacher");
		urnSpec.getActors().add(actor);

		Actor actor2 = factory.createActor();
		actor2.setName("Student");
		urnSpec.getActors().add(actor2);
		
		// INTENTIONAL ELEMENT
		IntentionalElement elem1 = factory.createIntentionalElement();
		elem1.setType(IntentionalElementType.SOFTGOAL);
		elem1.setName("Improve Performance");
		actor.getElems().add(elem1);

		IntentionalElement elem2 = factory.createIntentionalElement();
		elem2.setType(IntentionalElementType.GOAL);
		elem2.setName("Increase Trust");
		actor.getElems().add(elem2);

		IntentionalElement elem3 = factory.createIntentionalElement();
		elem3.setType(IntentionalElementType.GOAL);
		elem3.setName("Increase Knowledge");
		actor.getElems().add(elem3);
		
		IntentionalElement elem4 = factory.createIntentionalElement();
		elem4.setType(IntentionalElementType.BELIEF);
		elem4.setName("Inspire Students");
		actor.getElems().add(elem4);
		
		IntentionalElement elem5 = factory.createIntentionalElement();
		elem5.setType(IntentionalElementType.INDICATOR);
		elem5.setName("Better Output");
		actor2.getElems().add(elem5);
		
		IntentionalElement elem6 = factory.createIntentionalElement();
		elem6.setType(IntentionalElementType.TASK);
		elem6.setName("Better Delivery");
		actor.getElems().add(elem6);
		
		// MIXED COMBINATION OF ELEMENT LINKS
		Decomposition decomposition = factory.createDecomposition();
		decomposition.setDest(elem4);
		decomposition.setDecompositionType(DecompositionType.XOR);
		elem2.getLinksSrc().add(decomposition);
		
		Decomposition decomposition2 = factory.createDecomposition();
		decomposition2.setDest(elem4);
		decomposition2.setDecompositionType(DecompositionType.XOR);
		elem3.getLinksSrc().add(decomposition2);
		
		Contribution contribution = factory.createContribution();
		contribution.setDest(elem2);
		contribution.setQuantitativeContribution(65);
		elem1.getLinksSrc().add(contribution);

		Contribution contribution2 = factory.createContribution();
		contribution2.setDest(elem3);
		contribution2.setQuantitativeContribution(90);
		elem1.getLinksSrc().add(contribution2);
		
		Dependency dependency = factory.createDependency();
		dependency.setDest(elem5);
		elem3.getLinksSrc().add(dependency);

		Dependency dependency2 = factory.createDependency();
		dependency2.setDest(elem6);
		elem5.getLinksSrc().add(dependency2);
		
		// STRATEGY
		EvaluationStrategy strategy = factory.createEvaluationStrategy();
		strategy.setName("Strategy First");
		urnSpec.getStrategies().add(strategy);
		
		// EVALUATION
		Evaluation eval1 = factory.createEvaluation();
		eval1.setIntElement(elem1);
		eval1.setEvaluation(65);
		strategy.getEvaluations().add(eval1);
		
		Evaluation eval2 = factory.createEvaluation();
		eval2.setIntElement(elem6);
		eval2.setEvaluation(-70);
		strategy.getEvaluations().add(eval2);
		
		TURNEvaluationManager evalManager = new TURNEvaluationManager(strategy);
		
		assertEquals(65, evalManager.getEvaluation(elem1));
		assertEquals(42, evalManager.getEvaluation(elem2));
		assertEquals(-70, evalManager.getEvaluation(elem3));
		assertEquals(42, evalManager.getEvaluation(elem4));
		assertEquals(-70, evalManager.getEvaluation(elem5));
		assertEquals(-70, evalManager.getEvaluation(elem6));
	}
	
	
	
//	@Test
//	public void dependencyContributionLinksTwoLevels() {
//
//		TURNFactory factory = new TURNFactoryImpl();
//
//		// URNSPEC
//		URNspec urnSpec = factory.createURNspec();
//		urnSpec.setName("School");
//
//		// ACTOR
//		Actor actor = factory.createActor();
//		actor.setName("Teacher");
//		urnSpec.getActors().add(actor);
//		
//		Actor actor2 = factory.createActor();
//		actor2.setName("Student");
//		urnSpec.getActors().add(actor2);
//
//		// INTENTIONAL ELEMENT
//		IntentionalElement elem1 = factory.createIntentionalElement();
//		elem1.setType(IntentionalElementType.SOFTGOAL);
//		elem1.setName("Improve Performance");
//		actor.getElems().add(elem1);
//
//		IntentionalElement elem2 = factory.createIntentionalElement();
//		elem2.setType(IntentionalElementType.GOAL);
//		elem2.setName("Increase Trust");
//		actor.getElems().add(elem2);
//
//		IntentionalElement elem3 = factory.createIntentionalElement();
//		elem3.setType(IntentionalElementType.GOAL);
//		elem3.setName("Increase Knowledge");
//		actor.getElems().add(elem3);
//		
//		IntentionalElement elem4 = factory.createIntentionalElement();
//		elem4.setType(IntentionalElementType.BELIEF);
//		elem4.setName("Better Output");
//		actor2.getElems().add(elem4);
//		
//		IntentionalElement elem5 = factory.createIntentionalElement();
//		elem5.setType(IntentionalElementType.GOAL);
//		elem5.setName("Inspire Students");
//		actor2.getElems().add(elem5);
//		
//		IntentionalElement elem6 = factory.createIntentionalElement();
//		elem6.setType(IntentionalElementType.BELIEF);
//		elem6.setName("High Attendance");
//		actor.getElems().add(elem6);
//		
//		// DECOMPOSITION DEPENDENCY ELEMENT LINK
//		Decomposition decomposition = factory.createDecomposition();
//		decomposition.setDest(elem3);
//		decomposition.setDecompositionType(DecompositionType.AND);
//		elem1.getLinksSrc().add(decomposition);
//
//		Decomposition decomposition2 = factory.createDecomposition();
//		decomposition2.setDest(elem3);
//		decomposition2.setDecompositionType(DecompositionType.AND);
//		elem2.getLinksSrc().add(decomposition2);
//		
//		Dependency dependency1 = factory.createDependency();
//		dependency1.setDest(elem4);
//		elem3.getLinksSrc().add(dependency1);
//		
//		Decomposition decomposition3 = factory.createDecomposition();
//		decomposition3.setDest(elem6);
//		decomposition3.setDecompositionType(DecompositionType.OR);
//		elem3.getLinksSrc().add(decomposition3);
//		
//		Dependency dependency2 = factory.createDependency();
//		dependency2.setDest(elem5);
//		elem6.getLinksSrc().add(dependency2);
//		
//		// STRATEGY
//		EvaluationStrategy strategy = factory.createEvaluationStrategy();
//		strategy.setName("StrategyFirst");
//		urnSpec.getStrategies().add(strategy);
//
//		// EVALUATION
//		Evaluation eval1 = factory.createEvaluation();
//		eval1.setIntElement(elem1);
//		eval1.setEvaluation(80);
//		strategy.getEvaluations().add(eval1);
//		
//		Evaluation eval2 = factory.createEvaluation();
//		eval2.setIntElement(elem2);
//		eval2.setEvaluation(65);
//		strategy.getEvaluations().add(eval2);
//		
//		Evaluation eval3 = factory.createEvaluation();
//		eval3.setIntElement(elem4);
//		eval3.setEvaluation(70);
//		strategy.getEvaluations().add(eval3);
//
//		TURNEvaluationManager evalManager = new TURNEvaluationManager(strategy);
//
//		assertEquals(80, evalManager.getEvaluation(elem1));
//		assertEquals(65, evalManager.getEvaluation(elem2));
//		assertEquals(65, evalManager.getEvaluation(elem3));
//		assertEquals(70, evalManager.getEvaluation(elem4));
//		assertEquals(0, evalManager.getEvaluation(elem5));
//		assertEquals(0, evalManager.getEvaluation(elem6));
//	}
	
//	@Test
//	public void mixedElementLinks() {
//
//		TURNFactory factory = new TURNFactoryImpl();
//
//		// URNSPEC
//		URNspec urnSpec = factory.createURNspec();
//		urnSpec.setName("School");
//
//		// ACTOR
//		Actor actor = factory.createActor();
//		actor.setName("Teacher");
//		urnSpec.getActors().add(actor);
//
//		Actor actor2 = factory.createActor();
//		actor2.setName("Student");
//		urnSpec.getActors().add(actor2);
//		
//		// INTENTIONAL ELEMENT
//		IntentionalElement elem1 = factory.createIntentionalElement();
//		elem1.setType(IntentionalElementType.SOFTGOAL);
//		elem1.setName("Improve Performance");
//		actor.getElems().add(elem1);
//
//		IntentionalElement elem2 = factory.createIntentionalElement();
//		elem2.setType(IntentionalElementType.GOAL);
//		elem2.setName("Increase Trust");
//		actor.getElems().add(elem2);
//
//		IntentionalElement elem3 = factory.createIntentionalElement();
//		elem3.setType(IntentionalElementType.GOAL);
//		elem3.setName("Increase Knowledge");
//		actor.getElems().add(elem3);
//		
//		IntentionalElement elem4 = factory.createIntentionalElement();
//		elem4.setType(IntentionalElementType.BELIEF);
//		elem4.setName("Inspire Students");
//		actor2.getElems().add(elem4);
//		
//		IntentionalElement elem5 = factory.createIntentionalElement();
//		elem5.setType(IntentionalElementType.BELIEF);
//		elem5.setName("Better Output");
//		actor.getElems().add(elem5);
//		
//		IntentionalElement elem6 = factory.createIntentionalElement();
//		elem6.setType(IntentionalElementType.BELIEF);
//		elem6.setName("Better Delivery");
//		actor2.getElems().add(elem6);
//		
//		// MIXED COMBINATION OF ELEMENT LINKS
//		Decomposition decomposition = factory.createDecomposition();
//		decomposition.setDest(elem4);
//		decomposition.setDecompositionType(DecompositionType.XOR);
//		elem3.getLinksSrc().add(decomposition);
//		
//		Decomposition decomposition2 = factory.createDecomposition();
//		decomposition2.setDest(elem4);
//		decomposition2.setDecompositionType(DecompositionType.XOR);
//		elem5.getLinksSrc().add(decomposition2);
//		
//		Contribution contribution = factory.createContribution();
//		contribution.setDest(elem2);
//		contribution.setQuantitativeContribution(65);
//		elem1.getLinksSrc().add(contribution);
//
//		Contribution contribution2 = factory.createContribution();
//		contribution2.setDest(elem5);
//		contribution2.setQuantitativeContribution(90);
//		elem3.getLinksSrc().add(contribution2);
//		
//		Dependency dependency = factory.createDependency();
//		dependency.setDest(elem4);
//		elem2.getLinksSrc().add(dependency);
//
//		Dependency dependency2 = factory.createDependency();
//		dependency2.setDest(elem6);
//		elem1.getLinksSrc().add(dependency);
//		
//		// STRATEGY
//		EvaluationStrategy strategy = factory.createEvaluationStrategy();
//		strategy.setName("Strategy First");
//		urnSpec.getStrategies().add(strategy);
//		
//		// EVALUATION
//		Evaluation eval1 = factory.createEvaluation();
//		eval1.setIntElement(elem1);
//		eval1.setEvaluation(80);
//		strategy.getEvaluations().add(eval1);
//		
//		Evaluation eval2 = factory.createEvaluation();
//		eval2.setIntElement(elem3);
//		eval2.setEvaluation(85);
//		strategy.getEvaluations().add(eval2);
//		
//		Evaluation eval3 = factory.createEvaluation();
//		eval3.setIntElement(elem6);
//		eval3.setEvaluation(70);
//		strategy.getEvaluations().add(eval3);
//		
//		TURNEvaluationManager evalManager = new TURNEvaluationManager(strategy);
//		
//		assertEquals(80, evalManager.getEvaluation(elem1));
//		assertEquals(52, evalManager.getEvaluation(elem2));
//		assertEquals(85, evalManager.getEvaluation(elem3));
//		assertEquals(85, evalManager.getEvaluation(elem4));
//		assertEquals(76, evalManager.getEvaluation(elem5));
//		assertEquals(70, evalManager.getEvaluation(elem6));
//	}
	
	
		
//	@Test
//	public void decompositionContibutionLinksSecondLevel() {
//
//		TURNFactory factory = new TURNFactoryImpl();
//
//		// URNSPEC
//		URNspec urnSpec = factory.createURNspec();
//		urnSpec.setName("School");
//
//		// ACTOR
//		Actor actor = factory.createActor();
//		actor.setName("Teacher");
//		urnSpec.getActors().add(actor);
//
//		// INTENTIONAL ELEMENT
//		IntentionalElement elem = factory.createIntentionalElement();
//		elem.setType(IntentionalElementType.SOFTGOAL);
//		elem.setName("Improve Performance");
//		actor.getElems().add(elem);
//
//		IntentionalElement elem2 = factory.createIntentionalElement();
//		elem2.setType(IntentionalElementType.GOAL);
//		elem2.setName("Increase Trust");
//		actor.getElems().add(elem2);
//
//		IntentionalElement elem3 = factory.createIntentionalElement();
//		elem3.setType(IntentionalElementType.GOAL);
//		elem3.setName("Increase Knowledge");
//		actor.getElems().add(elem3);
//
//		IntentionalElement elem4 = factory.createIntentionalElement();
//		elem4.setType(IntentionalElementType.BELIEF);
//		elem4.setName("Inspire Students");
//		actor.getElems().add(elem4);
//
//		IntentionalElement elem5 = factory.createIntentionalElement();
//		elem5.setType(IntentionalElementType.BELIEF);
//		elem5.setName("Better Output");
//		actor.getElems().add(elem5);
//
//		IntentionalElement elem6 = factory.createIntentionalElement();
//		elem6.setType(IntentionalElementType.BELIEF);
//		elem6.setName("High Attendance");
//		actor.getElems().add(elem6);
//
//		// ELEMENT LINKS
//		Decomposition decomposition = factory.createDecomposition();
//		decomposition.setDest(elem3);
//		decomposition.setDecompositionType(DecompositionType.OR);
//		elem.getLinksSrc().add(decomposition);
//
//		Decomposition decomposition2 = factory.createDecomposition();
//		decomposition2.setDest(elem3);
//		decomposition2.setDecompositionType(DecompositionType.OR);
//		elem2.getLinksSrc().add(decomposition2);
//
//		Contribution contribution = factory.createContribution();
//		contribution.setDest(elem4);
//		contribution.setQuantitativeContribution(80);
//		elem3.getLinksSrc().add(contribution);
//
//		Contribution contribution2 = factory.createContribution();
//		contribution2.setDest(elem5);
//		contribution2.setQuantitativeContribution(90);
//		elem3.getLinksSrc().add(contribution2);
//
//		Decomposition decomposition3 = factory.createDecomposition();
//		decomposition3.setDest(elem6);
//		decomposition3.setDecompositionType(DecompositionType.AND);
//		elem4.getLinksSrc().add(decomposition3);
//
//		Decomposition decomposition4 = factory.createDecomposition();
//		decomposition4.setDest(elem6);
//		decomposition4.setDecompositionType(DecompositionType.AND);
//		elem5.getLinksSrc().add(decomposition4);
//
//		// STRATEGY
//		EvaluationStrategy strategy = factory.createEvaluationStrategy();
//		strategy.setName("StrategyFirst");
//		urnSpec.getStrategies().add(strategy);
//
//		// EVALUATION
//		Evaluation eval = factory.createEvaluation();
//		eval.setIntElement(elem);
//		eval.setEvaluation(80);
//		strategy.getEvaluations().add(eval);
//
//		Evaluation eval2 = factory.createEvaluation();
//		eval2.setIntElement(elem2);
//		eval2.setEvaluation(90);
//		strategy.getEvaluations().add(eval2);
//
//		TURNEvaluationManager evalManager = new TURNEvaluationManager(strategy);
//
//		assertEquals(90, evalManager.getEvaluation(elem3));
//		assertEquals(72, evalManager.getEvaluation(elem4));
//		assertEquals(81, evalManager.getEvaluation(elem5));
//		assertEquals(72, evalManager.getEvaluation(elem6));
//	}

//	@Test
//	public void decomposition() {
//
//		TURNFactory factory = new TURNFactoryImpl();
//
//		// URNSPEC
//		URNspec urnSpec = factory.createURNspec();
//		urnSpec.setName("School");
//
//		// ACTOR
//		Actor actor = factory.createActor();
//		actor.setName("Teacher");
//		urnSpec.getActors().add(actor);
//
//		// INTENTIONAL ELEMENT
//		IntentionalElement elem = factory.createIntentionalElement();
//		elem.setType(IntentionalElementType.SOFTGOAL);
//		elem.setName("ImprovePerformance");
//		actor.getElems().add(elem);
//
//		IntentionalElement elem2 = factory.createIntentionalElement();
//		elem2.setType(IntentionalElementType.GOAL);
//		elem2.setName("IncreaseTrust");
//		actor.getElems().add(elem2);
//
//		IntentionalElement elem3 = factory.createIntentionalElement();
//		elem3.setType(IntentionalElementType.GOAL);
//		elem3.setName("IncreaseKnowledge");
//		actor.getElems().add(elem3);
//		
//		IntentionalElement elem4 = factory.createIntentionalElement();
//		elem4.setType(IntentionalElementType.BELIEF);
//		elem4.setName("InspireStudents");
//		actor.getElems().add(elem4);
//		
//		IntentionalElement elem5 = factory.createIntentionalElement();
//		elem5.setType(IntentionalElementType.BELIEF);
//		elem5.setName("Better Output");
//		actor.getElems().add(elem5);
//
//		// DECOMPOSITION ELEMENT LINK
//		Decomposition decomposition = factory.createDecomposition();
//		decomposition.setDest(elem3);
//		decomposition.setDecompositionType(DecompositionType.OR);
//		elem.getLinksSrc().add(decomposition);
//		
//		Decomposition decomposition2 = factory.createDecomposition();
//		decomposition2.setDest(elem3);
//		decomposition2.setDecompositionType(DecompositionType.OR);
//		elem2.getLinksSrc().add(decomposition2);
//		
//		Decomposition decomposition3 = factory.createDecomposition();
//		decomposition3.setDest(elem5);
//		decomposition3.setDecompositionType(DecompositionType.AND);
//		elem3.getLinksSrc().add(decomposition3);
//		
//		Decomposition decomposition4 = factory.createDecomposition();
//		decomposition4.setDest(elem5);
//		decomposition4.setDecompositionType(DecompositionType.AND);
//		elem4.getLinksSrc().add(decomposition4);
//
//		// STRATEGY
//		EvaluationStrategy strategy = factory.createEvaluationStrategy();
//		strategy.setName("StrategyFirst");
//		urnSpec.getStrategies().add(strategy);
//
//		// EVALUATION
//		Evaluation eval = factory.createEvaluation();
//		eval.setIntElement(elem);
//		eval.setEvaluation(80);
//		strategy.getEvaluations().add(eval);
//		
//		Evaluation eval2 = factory.createEvaluation();
//		eval2.setIntElement(elem2);
//		eval2.setEvaluation(90);
//		strategy.getEvaluations().add(eval2);
//		
//		Evaluation eval3 = factory.createEvaluation();
//		eval3.setIntElement(elem4);
//		eval3.setEvaluation(75);
//		strategy.getEvaluations().add(eval3);
//
//		TURNEvaluationManager evalManager = new TURNEvaluationManager(strategy);
//
//		assertEquals(90, evalManager.getEvaluation(elem3));
//		assertEquals(75, evalManager.getEvaluation(elem5));
//	}

//	@Test
//	public void contributionLinksSecondLevel() {
//
//		TURNFactory factory = new TURNFactoryImpl();
//
//		// URNSPEC
//		URNspec urnSpec = factory.createURNspec();
//		urnSpec.setName("School");
//
//		// ACTOR
//		Actor actor = factory.createActor();
//		actor.setName("Teacher");
//		urnSpec.getActors().add(actor);
//
//		// INTENTIONAL ELEMENT
//		IntentionalElement elem = factory.createIntentionalElement();
//		elem.setType(IntentionalElementType.SOFTGOAL);
//		elem.setName("ImprovePerformance");
//		actor.getElems().add(elem);
//
//		IntentionalElement elem2 = factory.createIntentionalElement();
//		elem2.setType(IntentionalElementType.GOAL);
//		elem2.setName("IncreaseTrust");
//		actor.getElems().add(elem2);
//
//		IntentionalElement elem3 = factory.createIntentionalElement();
//		elem3.setType(IntentionalElementType.GOAL);
//		elem3.setName("IncreaseMotivation");
//		actor.getElems().add(elem3);
//		
//		IntentionalElement elem4 = factory.createIntentionalElement();
//		elem4.setType(IntentionalElementType.BELIEF);
//		elem4.setName("InspireStudents");
//		actor.getElems().add(elem4);
//		
//		IntentionalElement elem5 = factory.createIntentionalElement();
//		elem5.setType(IntentionalElementType.BELIEF);
//		elem5.setName("Better Output");
//		actor.getElems().add(elem5);
//		
//		// CONTRIBUTION ELEMENT LINK
//		Contribution contribution = factory.createContribution();
//		contribution.setDest(elem2);
//		contribution.setQuantitativeContribution(80);
//		elem.getLinksSrc().add(contribution);
//
//		Contribution contribution2 = factory.createContribution();
//		contribution2.setDest(elem3);
//		contribution2.setQuantitativeContribution(90);
//		elem2.getLinksSrc().add(contribution2);
//		
//		Contribution contribution3 = factory.createContribution();
//		contribution3.setDest(elem4);
//		contribution3.setQuantitativeContribution(60);
//		elem2.getLinksSrc().add(contribution3);
//		
//		Contribution contribution4 = factory.createContribution();
//		contribution4.setDest(elem5);
//		contribution4.setQuantitativeContribution(70);
//		elem4.getLinksSrc().add(contribution4);
//		
//		// STRATEGY
//		EvaluationStrategy strategy = factory.createEvaluationStrategy();
//		strategy.setName("StrategyFirst");
//		urnSpec.getStrategies().add(strategy);
//
//		// EVALUATION
//		Evaluation eval = factory.createEvaluation();
//		eval.setIntElement(elem);
//		eval.setEvaluation(75);
//		strategy.getEvaluations().add(eval);
//
//		TURNEvaluationManager evalManager = new TURNEvaluationManager(strategy);
//
//		assertEquals(60, evalManager.getEvaluation(elem2));
//		assertEquals(54, evalManager.getEvaluation(elem3));
//		assertEquals(36, evalManager.getEvaluation(elem4));
//		assertEquals(25, evalManager.getEvaluation(elem5));
//	}

//	@Test
//	public void contributionDecompositionLinks() {
//
//		TURNFactory factory = new TURNFactoryImpl();
//
//		// URNSPEC
//		URNspec urnSpec = factory.createURNspec();
//		urnSpec.setName("School");
//
//		// ACTOR
//		Actor actor = factory.createActor();
//		actor.setName("Teacher");
//		urnSpec.getActors().add(actor);
//
//		// INTENTIONAL ELEMENT
//		IntentionalElement elem = factory.createIntentionalElement();
//		elem.setType(IntentionalElementType.SOFTGOAL);
//		elem.setName("ImprovePerformance");
//		actor.getElems().add(elem);
//
//		IntentionalElement elem2 = factory.createIntentionalElement();
//		elem2.setType(IntentionalElementType.GOAL);
//		elem2.setName("IncreaseTrust");
//		actor.getElems().add(elem2);
//
//		IntentionalElement elem3 = factory.createIntentionalElement();
//		elem3.setType(IntentionalElementType.GOAL);
//		elem3.setName("IncreaseKnowledge");
//		actor.getElems().add(elem3);
//		
//		// CONTRIBUTION DECOMPOSITION ELEMENT LINK
//		Contribution contribution = factory.createContribution();
//		contribution.setDest(elem2);
//		contribution.setQuantitativeContribution(80);
//		elem.getLinksSrc().add(contribution);
//		
//		Decomposition decomposition = factory.createDecomposition();
//		decomposition.setDest(elem3);
//		decomposition.setDecompositionType(DecompositionType.XOR);
//		elem2.getLinksSrc().add(decomposition);
//
//		Decomposition decomposition2 = factory.createDecomposition();
//		decomposition2.setDest(elem3);
//		decomposition2.setDecompositionType(DecompositionType.XOR);
//		elem.getLinksSrc().add(decomposition2);
//		
//		// STRATEGY
//		EvaluationStrategy strategy = factory.createEvaluationStrategy();
//		strategy.setName("StrategyFirst");
//		urnSpec.getStrategies().add(strategy);
//
//		// EVALUATION
//		Evaluation eval = factory.createEvaluation();
//		eval.setIntElement(elem);
//		eval.setEvaluation(80);
//		strategy.getEvaluations().add(eval);
//
//		TURNEvaluationManager evalManager = new TURNEvaluationManager(strategy);
//
//		assertEquals(64, evalManager.getEvaluation(elem2));
//		assertEquals(80, evalManager.getEvaluation(elem3));
//	}

//	@Test
//	public void decompositionLinklist() {
//
//		TURNFactory factory = new TURNFactoryImpl();
//
//		// URNSPEC
//		URNspec urnSpec = factory.createURNspec();
//		urnSpec.setName("School");
//
//		// ACTOR
//		Actor actor = factory.createActor();
//		actor.setName("Teacher");
//		urnSpec.getActors().add(actor);
//
//		// INTENTIONAL ELEMENT
//		IntentionalElement elem = factory.createIntentionalElement();
//		elem.setType(IntentionalElementType.SOFTGOAL);
//		elem.setName("ImprovePerformance");
//		actor.getElems().add(elem);
//
//		IntentionalElement elem2 = factory.createIntentionalElement();
//		elem2.setType(IntentionalElementType.GOAL);
//		elem2.setName("IncreaseTrust");
//		actor.getElems().add(elem2);
//
//		IntentionalElement elem3 = factory.createIntentionalElement();
//		elem3.setType(IntentionalElementType.GOAL);
//		elem3.setName("IncreaseKnowledge");
//		actor.getElems().add(elem3);
//		
//		IntentionalElement elem4 = factory.createIntentionalElement();
//		elem4.setType(IntentionalElementType.BELIEF);
//		elem4.setName("Better Output");
//		actor.getElems().add(elem4);
//		
//
//		// DECOMPOSITION ELEMENT LINK
//		Decomposition decomposition = factory.createDecomposition();
//		decomposition.setDest(elem2);
//		decomposition.setDecompositionType(DecompositionType.OR);
//		elem.getLinksSrc().add(decomposition);
//		
//		Decomposition decomposition2 = factory.createDecomposition();
//		decomposition2.setDest(elem4);
//		decomposition2.setDecompositionType(DecompositionType.OR);
//		elem3.getLinksSrc().add(decomposition2);
//		
//		Decomposition decomposition3 = factory.createDecomposition();
//		decomposition3.setDest(elem4);
//		decomposition3.setDecompositionType(DecompositionType.OR);
//		elem.getLinksSrc().add(decomposition3);
//
//		// STRATEGY
//		EvaluationStrategy strategy = factory.createEvaluationStrategy();
//		strategy.setName("StrategyFirst");
//		urnSpec.getStrategies().add(strategy);
//
//		// EVALUATION
//		Evaluation eval = factory.createEvaluation();
//		eval.setIntElement(elem);
//		eval.setEvaluation(80);
//		strategy.getEvaluations().add(eval);
//		
//		Evaluation eval2 = factory.createEvaluation();
//		eval2.setIntElement(elem3);
//		eval2.setEvaluation(90);
//		strategy.getEvaluations().add(eval2);
//
//		TURNEvaluationManager evalManager = new TURNEvaluationManager(strategy);
//
//		assertEquals(80, evalManager.getEvaluation(elem2));
//		assertEquals(90, evalManager.getEvaluation(elem4));
//	}

//	@Test
//	public void dependencyLinkList() {
//
//		TURNFactory factory = new TURNFactoryImpl();
//
//		// URNSPEC
//		URNspec urnSpec = factory.createURNspec();
//		urnSpec.setName("School");
//
//		// ACTOR
//		Actor actor = factory.createActor();
//		actor.setName("Teacher");
//		urnSpec.getActors().add(actor);
//
//		Actor actor2 = factory.createActor();
//		actor2.setName("Student");
//		urnSpec.getActors().add(actor2);
//
//		// INTENTIONAL ELEMENT
//		IntentionalElement elem = factory.createIntentionalElement();
//		elem.setType(IntentionalElementType.SOFTGOAL);
//		elem.setName("ImprovePerformance");
//		actor.getElems().add(elem);
//
//		IntentionalElement elem2 = factory.createIntentionalElement();
//		elem2.setType(IntentionalElementType.GOAL);
//		elem2.setName("IncreaseTrust");
//		actor2.getElems().add(elem2);
//
//		IntentionalElement elem3 = factory.createIntentionalElement();
//		elem3.setType(IntentionalElementType.GOAL);
//		elem3.setName("IncreaseMotivation");
//		actor.getElems().add(elem3);
//
//		// DEPENDENCY ELEMENT LINK
//		
//		Dependency dependency = factory.createDependency();
//		dependency.setDest(elem2);
//		elem.getLinksSrc().add(dependency);
//		
//		Dependency dependency2 = factory.createDependency();
//		dependency2.setDest(elem3);
//		elem.getLinksSrc().add(dependency2);
//		
//		
//		// STRATEGY
//		EvaluationStrategy strategy = factory.createEvaluationStrategy();
//		strategy.setName("StrategyFirst");
//		urnSpec.getStrategies().add(strategy);
//
//		// EVALUATION
//		Evaluation eval = factory.createEvaluation();
//		eval.setIntElement(elem2);
//		eval.setEvaluation(50);
//		eval.setIntElement(elem3);
//		eval.setEvaluation(-75);
//		strategy.getEvaluations().add(eval);
//
//		TURNEvaluationManager evalManager = new TURNEvaluationManager(strategy);
//		//assertEquals(-75, evalManager.getEvaluation(elem));
//		assertEquals(0, evalManager.getEvaluation(elem));
//	}

//	@Test
//	public void contributionLinkSet() {
//
//		TURNFactory factory = new TURNFactoryImpl();
//
//		// URNSPEC
//		URNspec urnSpec = factory.createURNspec();
//		urnSpec.setName("School");
//
//		// ACTOR
//		Actor actor = factory.createActor();
//		actor.setName("Teacher");
//		urnSpec.getActors().add(actor);
//
//		// INTENTIONAL ELEMENT
//		IntentionalElement elem = factory.createIntentionalElement();
//		elem.setType(IntentionalElementType.SOFTGOAL);
//		elem.setName("ImprovePerformance");
//		actor.getElems().add(elem);
//
//		IntentionalElement elem2 = factory.createIntentionalElement();
//		elem2.setType(IntentionalElementType.GOAL);
//		elem2.setName("IncreaseTrust");
//		actor.getElems().add(elem2);
//
//		IntentionalElement elem3 = factory.createIntentionalElement();
//		elem3.setType(IntentionalElementType.GOAL);
//		elem3.setName("IncreaseMotivation");
//		actor.getElems().add(elem3);
//		
//		IntentionalElement elem4 = factory.createIntentionalElement();
//		elem4.setType(IntentionalElementType.GOAL);
//		elem4.setName("InspireStudents");
//		actor.getElems().add(elem4);
//
//		// CONTRIBUTION ELEMENT LINK
//		Contribution contribution = factory.createContribution();
//		contribution.setDest(elem2);
//		contribution.setQuantitativeContribution(80);
//		elem.getLinksSrc().add(contribution);
//
//		Contribution contribution2 = factory.createContribution();
//		contribution2.setDest(elem3);
//		contribution2.setQuantitativeContribution(90);
//		elem.getLinksSrc().add(contribution2);
//		
//		Contribution contribution3 = factory.createContribution();
//		contribution3.setDest(elem4);
//		contribution3.setQuantitativeContribution(90);
//		elem3.getLinksSrc().add(contribution3);
//
//		// STRATEGY
//		EvaluationStrategy strategy = factory.createEvaluationStrategy();
//		strategy.setName("StrategyFirst");
//		urnSpec.getStrategies().add(strategy);
//
//		// EVALUATION
//		Evaluation eval = factory.createEvaluation();
//		eval.setIntElement(elem);
//		eval.setEvaluation(80);
//		strategy.getEvaluations().add(eval);
//
//		TURNEvaluationManager evalManager = new TURNEvaluationManager(strategy);
//
//		assertEquals(64, evalManager.getEvaluation(elem2));
//		assertEquals(72, evalManager.getEvaluation(elem3));
//		assertEquals(72, evalManager.getEvaluation(elem3));
//	}

//	@Test
//	public void contriDependLinkSet() {
//
//		TURNFactory factory = new TURNFactoryImpl();
//
//		// URNSPEC
//		URNspec urnSpec = factory.createURNspec();
//		urnSpec.setName("School");
//
//		// ACTOR
//		Actor actor = factory.createActor();
//		actor.setName("Teacher");
//		urnSpec.getActors().add(actor);
//		
//		Actor actor2 = factory.createActor();
//		actor2.setName("Student");
//		urnSpec.getActors().add(actor2);
//
//		// INTENTIONAL ELEMENT
//		IntentionalElement elem = factory.createIntentionalElement();
//		elem.setType(IntentionalElementType.SOFTGOAL);
//		elem.setName("Improve Performance");
//		actor.getElems().add(elem);
//
//		IntentionalElement elem2 = factory.createIntentionalElement();
//		elem2.setType(IntentionalElementType.GOAL);
//		elem2.setName("Increase Trust");
//		actor.getElems().add(elem2);
//
//		IntentionalElement elem3 = factory.createIntentionalElement();
//		elem3.setType(IntentionalElementType.GOAL);
//		elem3.setName("Increase Motivation");
//		actor.getElems().add(elem3);
//		
//		IntentionalElement elem4 = factory.createIntentionalElement();
//		elem4.setType(IntentionalElementType.GOAL);
//		elem4.setName("Improve Learning");
//		actor2.getElems().add(elem4);
//		
//		IntentionalElement elem5 = factory.createIntentionalElement();
//		elem5.setType(IntentionalElementType.GOAL);
//		elem5.setName("Inspire Students");
//		actor.getElems().add(elem5);
//
//		
//		// CONTRIBUTION DEPENDENCY ELEMENT LINK
//		Contribution contribution = factory.createContribution();
//		contribution.setDest(elem2);
//		contribution.setQuantitativeContribution(80);
//		elem.getLinksSrc().add(contribution);
//		
//		Contribution contribution2 = factory.createContribution();
//		contribution2.setDest(elem3);
//		contribution2.setQuantitativeContribution(80);
//		elem2.getLinksSrc().add(contribution2);
//		
//		Dependency dependency = factory.createDependency();
//		dependency.setDest(elem4);
//		elem5.getLinksSrc().add(dependency);
//
//		// STRATEGY
//		EvaluationStrategy strategy = factory.createEvaluationStrategy();
//		strategy.setName("Strategy First");
//		urnSpec.getStrategies().add(strategy);
//
//		// EVALUATION
//		Evaluation eval = factory.createEvaluation();
//		eval.setIntElement(elem);
//		eval.setEvaluation(80);
//		eval.setIntElement(elem4);
//		eval.setEvaluation(60);
//		strategy.getEvaluations().add(eval);
//
//		TURNEvaluationManager evalManager = new TURNEvaluationManager(strategy);
//
//		assertEquals(64, evalManager.getEvaluation(elem2));
////		assertEquals(51, evalManager.getEvaluation(elem3));
////		assertEquals(0, evalManager.getEvaluation(elem5));
//	}

}
