package mcgill.xtext.turn.diagram

import org.xtext.example.mydsl.tURN.EvaluationStrategy
import org.eclipse.xtext.testing.util.ParseHelper
import org.xtext.example.mydsl.tURN.URNspec
import com.google.inject.Inject
import org.junit.Test
import static org.junit.Assert.assertThat
import org.junit.Assert

class test1Xtend {
	
	
	private EvaluationStrategy strategyName;
	
	@Inject
	ParseHelper<URNspec> parseHelper;
	
	@Inject
	TURNEvaluationManager manager;
	
	@Test
	def void firstTest() {
		val model = parseHelper.parse('''
		urnModel firstModel 
		
		 	strategy strategyA#"strategyA" {
		     firstModel.Teacher.Education 
		     evaluation 90 
		     }
			
			actor Teacher#"Teacher"{
		    	goal Education#"Education"{
		        importance high
		    	}
			}
			 ''') 
		val urnspec = model.actors.get(0);
		Assert.assertEquals(urnspec.name, 'Teacher');
		
    }
}