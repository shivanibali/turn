package mcgill.xtext.turn.diagram;

import com.google.inject.Inject;
import mcgill.xtext.turn.diagram.TURNEvaluationManager;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.testing.util.ParseHelper;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.junit.Assert;
import org.junit.Test;
import org.xtext.example.mydsl.tURN.Actor;
import org.xtext.example.mydsl.tURN.EvaluationStrategy;
import org.xtext.example.mydsl.tURN.URNspec;

@SuppressWarnings("all")
public class test1Xtend {
  private EvaluationStrategy strategyName;
  
  @Inject
  private ParseHelper<URNspec> parseHelper;
  
  @Inject
  private TURNEvaluationManager manager;
  
  @Test
  public void firstTest() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("urnModel firstModel ");
      _builder.newLine();
      _builder.newLine();
      _builder.append(" \t");
      _builder.append("strategy strategyA#\"strategyA\" {");
      _builder.newLine();
      _builder.append("     ");
      _builder.append("firstModel.Teacher.Education ");
      _builder.newLine();
      _builder.append("     ");
      _builder.append("evaluation 90 ");
      _builder.newLine();
      _builder.append("     ");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("actor Teacher#\"Teacher\"{");
      _builder.newLine();
      _builder.append("    \t");
      _builder.append("goal Education#\"Education\"{");
      _builder.newLine();
      _builder.append("        ");
      _builder.append("importance high");
      _builder.newLine();
      _builder.append("    \t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      final URNspec model = this.parseHelper.parse(_builder);
      final Actor urnspec = model.getActors().get(0);
      Assert.assertEquals(urnspec.getName(), "Teacher");
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
