package mcgill.xtext.turn.diagram;

import java.util.*;
import java.util.Map.Entry;

import org.xtext.example.mydsl.tURN.Actor;
import org.xtext.example.mydsl.tURN.Contribution;
import org.xtext.example.mydsl.tURN.Decomposition;
import org.xtext.example.mydsl.tURN.DecompositionType;
import org.xtext.example.mydsl.tURN.Dependency;
import org.xtext.example.mydsl.tURN.ElementLink;
import org.xtext.example.mydsl.tURN.Evaluation;
import org.xtext.example.mydsl.tURN.EvaluationStrategy;
import org.xtext.example.mydsl.tURN.ImportanceType;
import org.xtext.example.mydsl.tURN.IntentionalElement;
import org.xtext.example.mydsl.tURN.URNspec;

public class TURNEvaluationManager {

	private EvaluationStrategy strategy;
	private HashMap<IntentionalElement, Integer> evaluations = new HashMap<IntentionalElement, Integer>();
	private List<IntentionalElement> evalReady = new ArrayList<IntentionalElement>();
	private HashMap<IntentionalElement, Integer> counts = new HashMap<IntentionalElement, Integer>();
	private HashMap<IntentionalElement, List<ElementLink>> incomingLinks = new HashMap<IntentionalElement, List<ElementLink>>();
	private HashMap<IntentionalElement, List<Dependency>> incomingDependencyLinks = new HashMap<IntentionalElement, List<Dependency>>();

	TURNEvaluationManager(EvaluationStrategy strategy) {
		this.strategy = strategy;
		init();
		evaluateModel();
	}

	private void evaluateModel() {
		while (!evalReady.isEmpty()) {
			IntentionalElement ie = evalReady.remove(0);
			Integer evalValue = evaluations.get(ie);
			if (evalValue == null) {
				int result = calculate(ie, strategy);
				evaluations.put(ie, result);
			}
			for (ElementLink link : ie.getLinksSrc()) {
				if (link instanceof Contribution) {
					IntentionalElement dest = ((Contribution) link).getDest();
					int count = counts.get(dest) - 1;
					counts.put(dest, count);
					if (count == 0) {
						evalReady.add(dest);
					}
				}
				if (link instanceof Decomposition) {
					IntentionalElement dest = ((Decomposition) link).getDest();
					int count = counts.get(dest) - 1;
					counts.put(dest, count);
					if (count == 0) {
						evalReady.add(dest);
					}
				}
			}
			for (Dependency link : incomingDependencyLinks.get(ie)) {
				IntentionalElement source = (IntentionalElement) link.eContainer();
				int count = counts.get(source) - 1;
				counts.put(source, count);
				if (count == 0) {
					evalReady.add(source);
				}
			}
		}
	}

	public int getEvaluation(IntentionalElement ie) {
		Integer eval = evaluations.get(ie);
		return eval;
	}

	private void init() {
		for (Actor actor : ((URNspec) strategy.eContainer()).getActors()) {
			for (IntentionalElement ie : actor.getElems()) {
				counts.put(ie, 0);
				incomingLinks.put(ie, new ArrayList<ElementLink>());
				incomingDependencyLinks.put(ie, new ArrayList<Dependency>());
			}
		}
		for (Actor actor : ((URNspec) strategy.eContainer()).getActors()) {
			for (IntentionalElement ie : actor.getElems()) {
				for (ElementLink link : ie.getLinksSrc()) {
					if (link instanceof Contribution) {
						IntentionalElement dest = ((Contribution) link).getDest();
						counts.put(dest, counts.get(dest) + 1);
						List<ElementLink> listl = incomingLinks.get(dest);
						listl.add(link);
						incomingLinks.put(dest, listl);
					}
					if (link instanceof Decomposition) {
						IntentionalElement dest = ((Decomposition) link).getDest();
						counts.put(dest, counts.get(dest) + 1);
						List<ElementLink> listl = incomingLinks.get(dest);
						listl.add(link);
						incomingLinks.put(dest, listl);
					}
					if (link instanceof Dependency) {
						IntentionalElement source = (IntentionalElement) link.eContainer();
						counts.put(source, counts.get(source) + 1);
						IntentionalElement dest = ((Dependency) link).getDest();
						List<Dependency> depLinks = incomingDependencyLinks.get(dest);
						depLinks.add((Dependency) link);
						incomingDependencyLinks.put(dest, depLinks);
					}
				}
			}
		}
		for (Entry<IntentionalElement, Integer> e : counts.entrySet()) {
			if ((Integer) e.getValue() == 0) {
				evalReady.add((IntentionalElement) e.getKey());
				evaluations.put((IntentionalElement) e.getKey(), 0);
			}
		}
		for (Evaluation eval : strategy.getEvaluations()) {
			evaluations.put(eval.getIntElement(), eval.getEvaluation());
		}
	}

	private int calculate(IntentionalElement ie, EvaluationStrategy strategy) {
		int decompVal = -999;
		for (ElementLink link : incomingLinks.get(ie)) {
			if (link instanceof Decomposition) {
				IntentionalElement sourceElement = (IntentionalElement) link.eContainer();
				Integer evalVal = evaluations.get(sourceElement);
				if (((Decomposition) link).getDecompositionType() == DecompositionType.AND) {
					if (decompVal != -999)
						decompVal = Math.min(evalVal, decompVal);
					else
						decompVal = evalVal;
				} else if (((Decomposition) link).getDecompositionType() == DecompositionType.OR
						|| (((Decomposition) link).getDecompositionType() == DecompositionType.XOR)) {
					decompVal = Math.max(evalVal, decompVal);
				}
			}
		}

		int totalContrib = 0;
		for (ElementLink link : incomingLinks.get(ie)) {
			if (link instanceof Contribution) {
				Integer quantContri = ((Contribution) link).getQuantitativeContribution();
				IntentionalElement sourceElement = (IntentionalElement) link.eContainer();
				Integer evalVal = evaluations.get(sourceElement);
				int contribValue = evalVal * quantContri;
				totalContrib += contribValue / 100;
			}
		}

		if (decompVal != -999) {
			totalContrib += decompVal;
		}
		if ((totalContrib < -100)) {
			totalContrib = -100;
		} else if ((totalContrib > 100)) {
			totalContrib = 100;
		}
		
		int dependVal = totalContrib;


		for (ElementLink link : ie.getLinksSrc()) {
			if (link instanceof Dependency) {
				IntentionalElement destElement = ((Dependency) link).getDest();
				Integer evalDVal = evaluations.get(destElement);
				dependVal = Math.min(evalDVal, dependVal);
			}
		}
		return dependVal;
	}

}
