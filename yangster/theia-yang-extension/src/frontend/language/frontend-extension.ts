/*
 * Copyright (C) 2017 TypeFox and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License") you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 */

import { ContainerModule, interfaces } from 'inversify'
import { CommandContribution } from '@theia/core/lib/common'
import { LanguageClientContribution } from '@theia/languages/lib/browser'
import { TurnLanguageClientContribution } from './turn-language-client-contribution'
import { DiagramConfiguration } from 'theia-sprotty/lib'
import { TURNDiagramConfiguration } from '../turndiagram/di.config'
import { DiagramManager, DiagramManagerProvider } from 'theia-sprotty/lib'
import { TURNDiagramManager } from '../turndiagram/turn-diagram-manager'
import { FrontendApplicationContribution, OpenHandler } from '@theia/core/lib/browser'
import { configuration, monarchLanguage } from './turn-monaco-language'
import { TURNCommandContribution } from './turn-commands'
import { MonacoEditorProvider } from '@theia/monaco/lib/browser/monaco-editor-provider'
import { TurnMonacoEditorProvider } from "../monaco/turn-monaco-editor-provider"
import 'sprotty/css/sprotty.css'
import 'theia-sprotty/css/theia-sprotty.css'
import { ContextMenuCommands } from './dynamic-commands'
import { ThemeManager } from '../turndiagram/theme-manager';

export default new ContainerModule((bind: interfaces.Bind, unbind: interfaces.Unbind, isBound: interfaces.IsBound, rebind: interfaces.Rebind) => {
    monaco.languages.register({
        id: 'turn',
        aliases: ['Turn', 'turn'],
        extensions: ['.turn'],
        mimetypes: ['text/turn']
    })
    monaco.languages.onLanguage('turn', () => {
        monaco.languages.setLanguageConfiguration('turn', configuration)
        monaco.languages.setMonarchTokensProvider('turn', monarchLanguage)
    });
    bind(CommandContribution).to(TURNCommandContribution).inSingletonScope();
    bind(TurnLanguageClientContribution).toSelf().inSingletonScope()
    bind(LanguageClientContribution).toDynamicValue(ctx => ctx.container.get(TurnLanguageClientContribution))
    bind(DiagramConfiguration).to(TURNDiagramConfiguration).inSingletonScope()
    bind(DiagramManagerProvider).toProvider<DiagramManager>(context => {
        return () => {
            return new Promise<DiagramManager>((resolve) =>
                resolve(context.container.get(TURNDiagramManager))
            )
        }
    }).whenTargetNamed('turn-diagram')
    bind(TURNDiagramManager).toSelf().inSingletonScope()
    bind(FrontendApplicationContribution).toDynamicValue(context => context.container.get(TURNDiagramManager))
    bind(OpenHandler).toDynamicValue(context => context.container.get(TURNDiagramManager))
    bind(ContextMenuCommands).to(ContextMenuCommands).inSingletonScope()
    rebind(MonacoEditorProvider).to(TurnMonacoEditorProvider).inSingletonScope()
    bind(ThemeManager).toSelf().inSingletonScope()
})