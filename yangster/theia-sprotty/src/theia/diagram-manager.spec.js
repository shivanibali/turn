"use strict";
/*
 * Copyright (C) 2017 TypeFox and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License") you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
require("mocha");
var chai_1 = require("chai");
var inversify_1 = require("inversify");
var Connector = /** @class */ (function () {
    function Connector() {
        this.c = Connector_1.counter++;
    }
    Connector_1 = Connector;
    Connector.counter = 0;
    Connector = Connector_1 = __decorate([
        inversify_1.injectable()
    ], Connector);
    return Connector;
    var Connector_1;
}());
var AbstractManager = /** @class */ (function () {
    function AbstractManager() {
    }
    __decorate([
        inversify_1.inject(Connector)
    ], AbstractManager.prototype, "connector", void 0);
    AbstractManager = __decorate([
        inversify_1.injectable()
    ], AbstractManager);
    return AbstractManager;
}());
var Manager0 = /** @class */ (function (_super) {
    __extends(Manager0, _super);
    function Manager0() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Manager0 = __decorate([
        inversify_1.injectable()
    ], Manager0);
    return Manager0;
}(AbstractManager));
var Manager1 = /** @class */ (function (_super) {
    __extends(Manager1, _super);
    function Manager1() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Manager1 = __decorate([
        inversify_1.injectable()
    ], Manager1);
    return Manager1;
}(AbstractManager));
var m0 = Symbol('Manager0');
var m1 = Symbol('Manager1');
describe('inversify', function () {
    it('error', function () {
        var module = new inversify_1.ContainerModule(function (bind) {
            bind(Connector).to(Connector).inSingletonScope();
            bind(Manager0).to(Manager0).inSingletonScope();
            bind(Manager1).to(Manager1).inSingletonScope();
            bind(m0).toDynamicValue(function (c) { return c.container.get(Manager0); });
            bind(m1).toDynamicValue(function (c) { return c.container.get(Manager1); });
        });
        var container = new inversify_1.Container();
        container.load(module);
        var manager0 = container.get(Manager0);
        var manager1 = container.get(Manager1);
        chai_1.expect(manager0.connector.c).to.be.equal(manager1.connector.c);
        var m0inst = container.get(m0);
        var m1inst = container.get(m1);
        chai_1.expect(m0inst.connector.c).to.be.equal(m1inst.connector.c);
    });
});
